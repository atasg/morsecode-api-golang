package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Codes struct {
	ID     primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Letter string             `json:"letter,omitempty" bson:"letter,omitempty"`
	Code   string             `json:"code,omitempty" bson:"code,omitempty"`
}
