package controllers 

import(
	"morseCode/config"
	"morseCode/models"

	"context"
	"time"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"github.com/gofiber/fiber/v2"

)

func InsertTheCodes(c *fiber.Ctx) error{
	collection := config.MI.DB.Collection("codes")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	docs := []interface{}{
		bson.D{primitive.E{Key: "letter", Value: "A"},primitive.E{Key: "code", Value: ".-"}},
		bson.D{primitive.E{Key: "letter", Value: "B"},primitive.E{Key: "code", Value: "-..."}},
		bson.D{primitive.E{Key: "letter", Value: "C"},primitive.E{Key: "code", Value: "-.-."}},
		bson.D{primitive.E{Key: "letter", Value: "D"},primitive.E{Key: "code", Value: "-.."}},
		bson.D{primitive.E{Key: "letter", Value: "E"},primitive.E{Key: "code", Value: "."}},
		bson.D{primitive.E{Key: "letter", Value: "F"},primitive.E{Key: "code", Value: "..-."}},
		bson.D{primitive.E{Key: "letter", Value: "G"},primitive.E{Key: "code", Value: "--."}},
		bson.D{primitive.E{Key: "letter", Value: "H"},primitive.E{Key: "code", Value: "...."}},
		bson.D{primitive.E{Key: "letter", Value: "I"},primitive.E{Key: "code", Value: ".."}},
		bson.D{primitive.E{Key: "letter", Value: "J"},primitive.E{Key: "code", Value: ".---"}},
		bson.D{primitive.E{Key: "letter", Value: "K"},primitive.E{Key: "code", Value: "-.-"}},
		bson.D{primitive.E{Key: "letter", Value: "L"},primitive.E{Key: "code", Value: ".-.."}},
		bson.D{primitive.E{Key: "letter", Value: "M"},primitive.E{Key: "code", Value: "--"}},
		bson.D{primitive.E{Key: "letter", Value: "N"},primitive.E{Key: "code", Value: "-."}},
		bson.D{primitive.E{Key: "letter", Value: "O"},primitive.E{Key: "code", Value: "---"}},
		bson.D{primitive.E{Key: "letter", Value: "P"},primitive.E{Key: "code", Value: ".--."}},
		bson.D{primitive.E{Key: "letter", Value: "Q"},primitive.E{Key: "code", Value: "--.-"}},
		bson.D{primitive.E{Key: "letter", Value: "R"},primitive.E{Key: "code", Value: ".-."}},
		bson.D{primitive.E{Key: "letter", Value: "S"},primitive.E{Key: "code", Value: "..."}},
		bson.D{primitive.E{Key: "letter", Value: "T"},primitive.E{Key: "code", Value: "-"}},
		bson.D{primitive.E{Key: "letter", Value: "U"},primitive.E{Key: "code", Value: "..-"}},
		bson.D{primitive.E{Key: "letter", Value: "V"},primitive.E{Key: "code", Value: "...-"}},
		bson.D{primitive.E{Key: "letter", Value: "W"},primitive.E{Key: "code", Value: ".--"}},
		bson.D{primitive.E{Key: "letter", Value: "X"},primitive.E{Key: "code", Value: "-..-"}},
		bson.D{primitive.E{Key: "letter", Value: "Y"},primitive.E{Key: "code", Value: "-.--"}},
		bson.D{primitive.E{Key: "letter", Value: "Z"},primitive.E{Key: "code", Value: "--.."}},
		bson.D{primitive.E{Key: "letter", Value: "a"},primitive.E{Key: "code", Value: ".-"}},
		bson.D{primitive.E{Key: "letter", Value: "b"},primitive.E{Key: "code", Value: "-..."}},
		bson.D{primitive.E{Key: "letter", Value: "c"},primitive.E{Key: "code", Value: "-.-."}},
		bson.D{primitive.E{Key: "letter", Value: "d"},primitive.E{Key: "code", Value: "-.."}},
		bson.D{primitive.E{Key: "letter", Value: "e"},primitive.E{Key: "code", Value: "."}},
		bson.D{primitive.E{Key: "letter", Value: "f"},primitive.E{Key: "code", Value: "..-."}},
		bson.D{primitive.E{Key: "letter", Value: "g"},primitive.E{Key: "code", Value: "--."}},
		bson.D{primitive.E{Key: "letter", Value: "h"},primitive.E{Key: "code", Value: "...."}},
		bson.D{primitive.E{Key: "letter", Value: "i"},primitive.E{Key: "code", Value: ".."}},
		bson.D{primitive.E{Key: "letter", Value: "j"},primitive.E{Key: "code", Value: ".---"}},
		bson.D{primitive.E{Key: "letter", Value: "k"},primitive.E{Key: "code", Value: "-.-"}},
		bson.D{primitive.E{Key: "letter", Value: "l"},primitive.E{Key: "code", Value: ".-.."}},
		bson.D{primitive.E{Key: "letter", Value: "m"},primitive.E{Key: "code", Value: "--"}},
		bson.D{primitive.E{Key: "letter", Value: "n"},primitive.E{Key: "code", Value: "-."}},
		bson.D{primitive.E{Key: "letter", Value: "o"},primitive.E{Key: "code", Value: "---"}},
		bson.D{primitive.E{Key: "letter", Value: "p"},primitive.E{Key: "code", Value: ".--."}},
		bson.D{primitive.E{Key: "letter", Value: "q"},primitive.E{Key: "code", Value: "--.-"}},
		bson.D{primitive.E{Key: "letter", Value: "r"},primitive.E{Key: "code", Value: ".-."}},
		bson.D{primitive.E{Key: "letter", Value: "s"},primitive.E{Key: "code", Value: "..."}},
		bson.D{primitive.E{Key: "letter", Value: "t"},primitive.E{Key: "code", Value: "-"}},
		bson.D{primitive.E{Key: "letter", Value: "u"},primitive.E{Key: "code", Value: "..-"}},
		bson.D{primitive.E{Key: "letter", Value: "v"},primitive.E{Key: "code", Value: "...-"}},
		bson.D{primitive.E{Key: "letter", Value: "w"},primitive.E{Key: "code", Value: ".--"}},
		bson.D{primitive.E{Key: "letter", Value: "x"},primitive.E{Key: "code", Value: "-..-"}},
		bson.D{primitive.E{Key: "letter", Value: "y"},primitive.E{Key: "code", Value: "-.--"}},
		bson.D{primitive.E{Key: "letter", Value: "z"},primitive.E{Key: "code", Value: "--.."}},

	}

	result, err := collection.InsertMany(ctx, docs)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
            "success": false,
            "message": "Codes failed to insert",
            "error":   err,
        })
	}

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
        "data":    result,
        "success": true,
        "message": "Codes inserted successfully",
    })

}

func Encode(c *fiber.Ctx) error{

	collection := config.MI.DB.Collection("codes")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	givenSentence := c.Params("value")
	givenSentence = strings.Replace(givenSentence,"%20"," ",-1)
	words := strings.Fields(givenSentence)

	var listSentence [][]string
	for i := 0; i<len(words);i++{
		var listWords []string
		for _, s := range words[i]{

			var result bson.M
			
			err := collection.FindOne(ctx, bson.D{{"letter", string(s)}}).Decode(&result)
			if err != nil {
				return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
					"success": false,
					"message": "Morse Code Not Found From Database (1)",
					"error":   err,
				})
			}
			
			var letterCode models.Codes
			
			bsonBytes, _ := bson.Marshal(result)
			bson.Unmarshal(bsonBytes,&letterCode)

			listWords = append(listWords,letterCode.Code)

		}
		listSentence = append(listSentence,listWords)
	}

	result := ""
	for a:=0 ; a<len(listSentence);a++{
		word := listSentence[a]
		for b:=0;b<len(word);b++{
			result = result + word[b] + " "
		}
		if a != len(listSentence)-1 {
			result = result + " / "
		}
		
	}
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"data": result,
	})

}
