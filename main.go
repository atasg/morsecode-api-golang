package main 

import (
	"log"
	"os"

	"morseCode/config"
	"morseCode/routes"
	

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
)

func setupRoutes(app *fiber.App) {
    app.Get("/", func(c *fiber.Ctx) error {
        return c.Status(fiber.StatusOK).JSON(fiber.Map{
            "success":     true,
            "message":     "Root Endpoint",
        })
    })

    routes.MorseCodeRouters(app.Group("/morsecode"))
}


func main(){

	if os.Getenv("APP_ENV") != "production" {
        err := godotenv.Load()
        if err != nil {
            log.Fatal("Error loading .env file")
        }
    }

	app := fiber.New()
	app.Get("/", func(c *fiber.Ctx) error{
		return c.SendString("Hello!")
	})
	config.ConnectDB()

	setupRoutes(app)

	port:= os.Getenv("PORT")
	err := app.Listen(":" + port)

	if err != nil{
		log.Fatal("App failed to start")
		panic(err)
	}

}