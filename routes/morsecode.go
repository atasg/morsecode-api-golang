package routes

import (
	"github.com/gofiber/fiber/v2"

	"morseCode/controllers"
)


func MorseCodeRouters(route fiber.Router){
	route.Get("/insertdata", controllers.InsertTheCodes)
	route.Get("/encode/:value", controllers.Encode)
}